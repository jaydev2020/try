import { Ifo } from './types'

const ifos: Ifo[] = [
  {
    id: 'IFO',
    address: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
    isActive: true,
    name: 'LongSwap',
    subTitle: 'LongSwap - The Best Modern Yield Farm on Binance Smart Chain',
    description:
      'LongSwap - The Best Modern Yield Farm on Binance Smart Chain.',
    launchDate: 'February. 10',
    launchTime: '8AM UTC',
    saleAmount: '1,000,000 LONG',
    raiseAmount: '$000,000',
    pizzaToBurn: '$000,000',
    projectSiteUrl: 'https://longswap.org',
    currency: 'LONG-BNB LP',
    currencyAddress: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
    tokenDecimals: 18,
    releaseBlockNumber: 4086064,
  },
]

export default ifos
