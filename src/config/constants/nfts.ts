import { Nft } from './types'

export const RABBIT_MINTING_FARM_ADDRESS = '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3'
export const PIZZA_RABBITS_ADDRESS = '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3'

const Nfts: Nft[] = [
  {
    name: 'Swapsies',
    description: 'These bunnies love nothing more than swapping longswap. Especially on BSC.',
    originalImage: 'https://gateway.pinata.cloud/ipfs/QmXdHqg3nywpNJWDevJQPtkz93vpfoHcZWQovFz2nmtPf5/swapsies.png',
    previewImage: 'swapsies-preview.png',
    blurImage: 'swapsies-blur.png',
    sortOrder: 999,
    bunnyId: 0,
  },
]

export default Nfts
